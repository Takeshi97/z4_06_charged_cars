package electric.cars.chargedcars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class RecuperarPassActivity extends AppCompatActivity {

    private EditText edCorreoRecuperar;

    private FirebaseAuth mAuth;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_pass);


        edCorreoRecuperar = findViewById(R.id.edCorreoRecuperar);

        String correo_recu = getIntent().getStringExtra("correo_recu");
        edCorreoRecuperar.setText(correo_recu);



        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode("es");
        mDialog = new ProgressDialog(this);


        //mAuth = FirebaseAuth.getInstance();
    }



    public void envioCorreoRecuperar(View view){

        String email = edCorreoRecuperar.getText().toString();
        if(!email.isEmpty()){

            mDialog.setMessage("Estamos trabajando para usted ...");
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();

            mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if(task.isSuccessful()){
                        // Aqui va si se envio el correo
                        mDialog.dismiss();

                        Intent intent = new Intent(RecuperarPassActivity.this, LoginActivity.class);
                        startActivity(intent);
                        Toast.makeText(RecuperarPassActivity.this, "Se Envio el Correo de Recuperacion de Contraseña", Toast.LENGTH_LONG).show();

                    }else{

                        Log.w("ErrorSendEmail", task.getException().toString());
                        mDialog.dismiss();
                        Toast.makeText(RecuperarPassActivity.this, "No se puede enviar el correo", Toast.LENGTH_LONG).show();
                    }


                }
            });

        }else{

            edCorreoRecuperar.setError("Este campo es requerido");
        }
    }


}