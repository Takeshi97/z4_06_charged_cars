package electric.cars.chargedcars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

public class InfoRecargaActivity extends AppCompatActivity {

    private ImageView imagenRecargaInfo;
    private TextView txtNombreRecargaInfo;
    private TextView txtGrupoRecargaInfo;
    private TextView txtDisponRecargaInfo;
    private TextView txtDireccRecargaInfo;
    private TextView txtTelefoRecargaInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_recarga);

        String nombre = getIntent().getStringExtra("nombre");
        String dispo = getIntent().getStringExtra("dispo");
        //String direct = getIntent().getStringExtra("direct");
        //String telef = getIntent().getStringExtra("telef");
        //String grupo = getIntent().getStringExtra("grupo");
        int logo = getIntent().getIntExtra("logo", 0);



        imagenRecargaInfo = findViewById(R.id.imagenRecargaInfo);
        txtNombreRecargaInfo = findViewById(R.id.txtNombreRecargaInfo);
        txtGrupoRecargaInfo = findViewById(R.id.txtGrupoRecargaInfo);
        txtDisponRecargaInfo = findViewById(R.id.txtDisponRecargaInfo);
        txtDireccRecargaInfo = findViewById(R.id.txtDireccRecargaInfo);
        txtTelefoRecargaInfo = findViewById(R.id.txtTelefoRecargaInfo);



        imagenRecargaInfo.setImageResource(logo);
        txtNombreRecargaInfo.setText(nombre);
        txtGrupoRecargaInfo.setText(getIntent().getStringExtra("grupo"));
        txtDisponRecargaInfo.setText(dispo);
        txtDireccRecargaInfo.setText(getIntent().getStringExtra("direct"));
        txtTelefoRecargaInfo.setText(getIntent().getStringExtra("telef"));


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.ItemSalir:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}