package electric.cars.chargedcars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;

public class RegisterUserActivity extends AppCompatActivity {

    private EditText edCorreoRegistro, edPassRegistro;

    private FirebaseAuth mAuth;

    private EditText edFechaCumple, edNombreRegistro, edApellidoRegistro, edNewPassRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);



        edCorreoRegistro = findViewById(R.id.edCorreoRegistro);
        edPassRegistro = findViewById(R.id.edPassRegistro);

        mAuth = FirebaseAuth.getInstance();

        edFechaCumple = findViewById(R.id.edFechaCumple);

        edNombreRegistro = findViewById(R.id.edNombreRegistro);
        edApellidoRegistro = findViewById(R.id.edApellidoRegistro);
        edNewPassRegistro = findViewById(R.id.edNewPassRegistro);
    }



    public void registrarUsuarios(View view){

        String email = edCorreoRegistro.getText().toString();
        String password = edPassRegistro.getText().toString();
        String nombre = edNombreRegistro.getText().toString();
        String apellido = edApellidoRegistro.getText().toString();
        String confirmPass = edNewPassRegistro.getText().toString();
        String date = edFechaCumple.getText().toString();


        if(!email.isEmpty() && !password.isEmpty() && !nombre.isEmpty() && !apellido.isEmpty() && !confirmPass.isEmpty() && !date.isEmpty()){

            if(!password.equals(confirmPass)){

                edNewPassRegistro.setError("Las contraseñas no coinciden");
                Toast.makeText(RegisterUserActivity.this, "Las contraseñas Ingresadas no coinciden", Toast.LENGTH_SHORT).show();

            }else {

                    mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                /*          // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);                                                         */

                                    // TODO SE DEBE AGREGAR A LA BASE DE DATOS A FUTURO

                                    Intent intent = new Intent(RegisterUserActivity.this, HomeUserActivity.class);
                                    startActivity(intent);
                                    finish();

                                } else {
                /*          // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);                                                         */

                                    Toast.makeText(RegisterUserActivity.this, "Error al registrar usuario", Toast.LENGTH_SHORT).show();

                                }
                            }
                        });
            }



        }else if(email.isEmpty() && password.isEmpty() && nombre.isEmpty() && apellido.isEmpty() && confirmPass.isEmpty() && date.isEmpty()){


            edCorreoRegistro.setError("Correo Requerido");
            edPassRegistro.setError("Contraseña Requerida");
            edNombreRegistro.setError("Nombre Requerido");
            edApellidoRegistro.setError("Apellido Requerido");
            edNewPassRegistro.setError("Confirmacion de Contraseña Requerida");
            //edFechaCumple.setError("Fecha de nacimiento Requerida");

            Toast.makeText(RegisterUserActivity.this, "Los campos no pueden estar vacios", Toast.LENGTH_SHORT).show();
            Toast.makeText(RegisterUserActivity.this, "Fecha de nacimiento Requerida", Toast.LENGTH_SHORT).show();

        }if(email.isEmpty()){

            edCorreoRegistro.setError("Correo Requerido");
            //Toast.makeText(RegisterUserActivity.this, "Correo Requerido", Toast.LENGTH_SHORT).show();

        }if(password.isEmpty()){

            edPassRegistro.setError("Contraseña Requerida");
            //Toast.makeText(RegisterUserActivity.this, "Contraseña Requerida", Toast.LENGTH_SHORT).show();

        }if(nombre.isEmpty()){

            edNombreRegistro.setError("Nombre Requerido");
            //Toast.makeText(RegisterUserActivity.this, "Nombre Requerido", Toast.LENGTH_SHORT).show();

        }if(apellido.isEmpty()){

            edApellidoRegistro.setError("Apellido Requerido");
            //Toast.makeText(RegisterUserActivity.this, "Apellido Requerido", Toast.LENGTH_SHORT).show();

        }if(confirmPass.isEmpty()){

            edNewPassRegistro.setError("Confirmacion de Contraseña Requerida");
            //Toast.makeText(RegisterUserActivity.this, "Confirmacion de Contraseña Requerida", Toast.LENGTH_SHORT).show();

        }if(date.isEmpty()){

            //edFechaCumple.setError("Fecha de nacimiento Requerida");
            Toast.makeText(RegisterUserActivity.this, "Fecha de nacimiento Requerida", Toast.LENGTH_SHORT).show();
        }


    }





    public void obtenerFechaCumple(View view){

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayofmonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_DARK,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {

                System.out.println(year + "/" + (month+1) + "/" + dayOfMonth);

                edFechaCumple.setText(year + "/" + (month+1) + "/" + dayOfMonth);

            }
        }, year,month,dayofmonth);

        datePickerDialog.show();
    }
}