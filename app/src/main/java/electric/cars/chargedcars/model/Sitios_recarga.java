package electric.cars.chargedcars.model;

public class Sitios_recarga {

    private String nombre_punto, direccion_punto, telefono_punto, disponibilidad_punto, grupo_punto;
    private int id_Logo;



    public Sitios_recarga(String nombre_punto, String direccion_punto, String telefono_punto, String disponibilidad_punto, String grupo_punto,int id_Logo) {

        this.nombre_punto = nombre_punto;
        this.direccion_punto = direccion_punto;
        this.telefono_punto = telefono_punto;
        this.disponibilidad_punto = disponibilidad_punto;
        this.grupo_punto = grupo_punto;
        this.id_Logo = id_Logo;
    }

    public String getGrupo_punto() {
        return grupo_punto;
    }

    public void setGrupo_punto(String grupo_punto) {
        this.grupo_punto = grupo_punto;
    }

    public String getNombre_punto() {
        return nombre_punto;
    }

    public void setNombre_punto(String nombre_punto) {
        this.nombre_punto = nombre_punto;
    }

    public String getDireccion_punto() {
        return direccion_punto;
    }

    public void setDireccion_punto(String direccion_punto) {
        this.direccion_punto = direccion_punto;
    }

    public String getTelefono_punto() {
        return telefono_punto;
    }

    public void setTelefono_punto(String telefono_punto) {
        this.telefono_punto = telefono_punto;
    }

    public String getDisponibilidad_punto() {
        return disponibilidad_punto;
    }

    public void setDisponibilidad_punto(String disponibilidad_punto) {
        this.disponibilidad_punto = disponibilidad_punto;
    }

    public int getId_Logo() {
        return id_Logo;
    }

    public void setId_Logo(int id_Logo) {
        this.id_Logo = id_Logo;
    }

    @Override
    public String toString() {
        return "Sitios_recarga{" +
                "nombre_punto='" + nombre_punto + '\'' +
                ", direccion_punto='" + direccion_punto + '\'' +
                ", telefono_punto='" + telefono_punto + '\'' +
                ", disponibilidad_punto='" + disponibilidad_punto + '\'' +
                ", grupo_punto='" + grupo_punto + '\'' +
                ", id_Logo=" + id_Logo +
                '}';
    }
}
