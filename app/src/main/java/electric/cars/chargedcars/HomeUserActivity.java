package electric.cars.chargedcars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;

import java.util.LinkedList;

import electric.cars.chargedcars.adapter.Puntos_recarga_Adapter;
import electric.cars.chargedcars.model.Sitios_recarga;

public class HomeUserActivity extends AppCompatActivity {

    private RecyclerView recyclerRecarga;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_user);

        recyclerRecarga = findViewById(R.id.recyclerRecarga);

        LinkedList<Sitios_recarga> puntosRecarga = new LinkedList<Sitios_recarga>();
        puntosRecarga.add(new Sitios_recarga("Toyota Electric","Cr 7 # 12 - 75","451205478","Disponible","Grupo Toyota",R.drawable.toyota_logo));
        puntosRecarga.add(new Sitios_recarga("Mitsubishi Charged","Cr 9 # 36 - 77","10547632","Disponible","Grupo Mitsubishi",R.drawable.mitsubishi_logo));
        puntosRecarga.add(new Sitios_recarga("Subaru Electric","Cr 75 # 18 - 42","810348916","Disponible","Grupo Toyota",R.drawable.subaru_logo));
        puntosRecarga.add(new Sitios_recarga("Tesla Charged","Cr 21 # 24 - 67","7013249851","Disponible","Grupo Tesla",R.drawable.tesla_logo));
        puntosRecarga.add(new Sitios_recarga("Renault Electric","Tr 34 # 37 - 97","624589137","Disponible","Grupo Renault",R.drawable.renault_logo));
        puntosRecarga.add(new Sitios_recarga("Ultra Carga","Cr 85 # 37 - 95","378154601","Disponible","Grupo General Electric",R.drawable.ultra_carga_logo));
        puntosRecarga.add(new Sitios_recarga("Volter Car","Cr 110 # 97 - 79","2514873645","No Disponible","Grupo Volter",R.drawable.volter_car_logo));

        Puntos_recarga_Adapter adapter = new Puntos_recarga_Adapter(puntosRecarga);
        recyclerRecarga.setLayoutManager(new LinearLayoutManager(this));
        recyclerRecarga.setAdapter(adapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_general, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.ItemSalir:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}






















