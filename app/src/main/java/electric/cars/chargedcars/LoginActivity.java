package electric.cars.chargedcars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private EditText edCorreoLogin, edPassLogin;
    //rivate Button btnIniciar;
    private FirebaseAuth mAuth;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        edCorreoLogin = findViewById(R.id.edCorreoLogin);
        edPassLogin = findViewById(R.id.edPassLogin);
        //btnIniciar = findViewById(R.id.btnIniciar);


        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode("es");
        mDialog = new ProgressDialog(this);


        //mAuth = FirebaseAuth.getInstance();
    }



    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){

            lanzarHomeUser(currentUser);

            /*
            Intent intent = new Intent(this, HomeUserActivity.class);
            intent.putExtra("email", currentUser.getEmail());
            startActivity(intent);
            finish();                              */
        }
    }



    public void iniciarSesion(View view){

        String email = edCorreoLogin.getText().toString();
        String password = edPassLogin.getText().toString();


        if(!email.isEmpty() && !password.isEmpty()){

                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d("LoginSuccess", "signInWithEmail:success");
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        //updateUI(user);
                                        lanzarHomeUser(user);
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w("LoginError", "signInWithEmail:failure", task.getException());
                                        Toast.makeText(LoginActivity.this, /*"Authentication failed."*/"Inicio de Sesion Fallido",
                                                Toast.LENGTH_SHORT).show();
                                        //updateUI(null);
                                    }
                                }
                            });


        } else if(email.isEmpty() && password.isEmpty()){


            edCorreoLogin.setError("Correo Requerido");
            edPassLogin.setError("Contraseña Requerida");
            Toast.makeText(LoginActivity.this, "Los campos no pueden estar vacios", Toast.LENGTH_SHORT).show();

        }else if(email.isEmpty()){

            edCorreoLogin.setError("Correo Requerido");
            Toast.makeText(LoginActivity.this, "Correo Requerido", Toast.LENGTH_SHORT).show();

        }else if(password.isEmpty()){

            edPassLogin.setError("Contraseña Requerida");
            Toast.makeText(LoginActivity.this, "Contraseña Requerida", Toast.LENGTH_SHORT).show();
        }





    }



    private void lanzarHomeUser(FirebaseUser currentUser){

        Intent intent = new Intent(this, HomeUserActivity.class);
        intent.putExtra("email", currentUser.getEmail());
        startActivity(intent);
        finish();
    }



    public void registrarse(View view){

        Intent intent = new Intent(LoginActivity.this, RegisterUserActivity.class);
        startActivity(intent);
    }



    public void olvidoContraseña(View view){

        Intent intent = new Intent(LoginActivity.this, RecuperarPassActivity.class);
        intent.putExtra("correo_recu", edCorreoLogin.getText().toString());
        startActivity(intent);



    }


    public void hackerLogin(View view){

        Intent intent = new Intent(LoginActivity.this, HomeUserActivity.class);
        startActivity(intent);
    }


}