package electric.cars.chargedcars.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import electric.cars.chargedcars.InfoRecargaActivity;
import electric.cars.chargedcars.R;
import electric.cars.chargedcars.model.Sitios_recarga;

public class Puntos_recarga_Adapter extends RecyclerView.Adapter<Puntos_recarga_Adapter.ViewHolder> {

    private static List<Sitios_recarga> puntosRecargaList;

    public Puntos_recarga_Adapter(List<Sitios_recarga> puntosRecargaList) {

        this.puntosRecargaList = puntosRecargaList;

    }

    @Override
    public int getItemCount() {

        return puntosRecargaList.size();
    }


    @NonNull
    @Override
    public Puntos_recarga_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_recarga, parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Puntos_recarga_Adapter.ViewHolder holder, int position) {

        holder.txtNombreCard.setText(puntosRecargaList.get(position).getNombre_punto());
        holder.txtDispoCard.setText(puntosRecargaList.get(position).getDisponibilidad_punto());
        //holder.txtDirectCard.setText(puntosRecargaList.get(position).getDireccion_punto());
        //holder.txtTelefCard.setText(puntosRecargaList.get(position).getTelefono_punto());
        holder.txtGrupoCard.setText(puntosRecargaList.get(position).getGrupo_punto());

        holder.imgLogoCard.setImageResource(puntosRecargaList.get(position).getId_Logo());

        holder.position = holder.getAdapterPosition();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgLogoCard;
        private TextView txtNombreCard, txtDispoCard, txtDirectCard, txtTelefCard, txtGrupoCard;

        private int position;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLogoCard = itemView.findViewById(R.id.imgLogoCard);
            txtNombreCard = itemView.findViewById(R.id.txtNombreCard);
            txtDispoCard = itemView.findViewById(R.id.txtDispoCard);
            //txtDirectCard = itemView.findViewById(R.id.txtDirectCard);
            //txtTelefCard = itemView.findViewById(R.id.txtTelefCard);
            txtGrupoCard = itemView.findViewById(R.id.txtGrupoCard);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(view.getContext(), InfoRecargaActivity.class);
                    intent.putExtra("nombre", puntosRecargaList.get(position).getNombre_punto());
                    intent.putExtra("dispo", puntosRecargaList.get(position).getDisponibilidad_punto());
                    intent.putExtra("direct", puntosRecargaList.get(position).getDireccion_punto());
                    intent.putExtra("telef", puntosRecargaList.get(position).getTelefono_punto());
                    intent.putExtra("grupo", puntosRecargaList.get(position).getGrupo_punto());
                    intent.putExtra("logo", puntosRecargaList.get(position).getId_Logo());
                    view.getContext().startActivity(intent);
                    //Toast.makeText(view.getContext(), "Click en cardView " + position, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}












































